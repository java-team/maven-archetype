#!/bin/sh -e

for pom in $(find archetype-common archetype-models -iname 'pom.xml')
do
	sed -i -e'/<artifactId>modello-maven-plugin<\/artifactId>/a <version>1.4.1<\/version>' $pom
done
